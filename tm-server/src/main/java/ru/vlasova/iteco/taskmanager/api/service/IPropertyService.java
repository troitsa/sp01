package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    Integer getSessionLifetime();

    @NotNull
    String getSecretKey();

}
