package ru.vlasova.iteco.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {

    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @Nullable
    public static Date parseDateFromString(@Nullable String dateString){
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date result;
        try {
            result = format.parse(dateString);
        } catch (ParseException e){
            return null;
        }
        return result;
    }

    @NotNull private static final SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

    @NotNull
    public static String parseDateToString(@Nullable final Date date){
        return date != null ? dateFormat.format(date) : "undefined";
    }

    public static java.sql.Date parseDateToSQLDate(@Nullable final Date date) {
        if(date == null) return null;
        return new java.sql.Date(date.getTime());
    }

    @Nullable
    public static XMLGregorianCalendar toXMLGregorianCalendar(@NotNull final Date date) throws Exception {
        @NotNull final GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        @Nullable XMLGregorianCalendar xmlCalendar = null;
        xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        return xmlCalendar;
    }

    @Nullable
    public static Date toDate(@Nullable final XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }

}