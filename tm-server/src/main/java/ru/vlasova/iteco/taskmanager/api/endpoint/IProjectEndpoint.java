package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.dto.ProjectDTO;
import ru.vlasova.iteco.taskmanager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    ProjectDTO insertProject(@Nullable String token, @Nullable final String userId,
                          @Nullable final String name, @Nullable final String description,
                          @Nullable final String dateStart, @Nullable final String dateFinish) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjects(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjectsByUserId(@Nullable final String token,
                                          @NotNull String userId) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findOneProject(@Nullable final String token,
                           @NotNull String id) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findOneProjectByUserId(@Nullable final String token,
                                   @NotNull String userId,
                                   @NotNull String id) throws Exception;

    @WebMethod
    void persistProject(@Nullable final String token,
                           @NotNull ProjectDTO project) throws Exception;

    @WebMethod
    void mergeProject(@Nullable final String token,
                      @NotNull ProjectDTO project) throws Exception;

    @WebMethod
    void removeProjectById(@Nullable final String token,
                           @NotNull String id) throws Exception;

    @WebMethod
    void removeProjectByUserId(@Nullable final String token,
                               @NotNull String userId,
                               @NotNull String id) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final String token) throws Exception;

    @WebMethod
    void removeAllProjectByUserId(@Nullable final String token,
                                  @NotNull String userId) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO getProjectByIndex(@Nullable final String token,
                              @Nullable final String userId, int index) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> getTasksByProjectIndex(@Nullable final String token,
                                         @Nullable final String userId, int projectIndex) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> searchProject(@Nullable final String token,
                                @Nullable final String userId,
                                @Nullable final String searchString) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> sortProject(@Nullable final String userId,
                                 @Nullable final String sortMode) throws Exception;

}