package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;

@Component
@NoArgsConstructor
public class AbstractEndpoint {

    @Autowired
    protected ISessionService sessionService;

    protected void validateSession(@Nullable final String token) throws Exception {
        if (token == null) throw new Exception("You are not authorized");
        sessionService.validate(token);
    }

}
