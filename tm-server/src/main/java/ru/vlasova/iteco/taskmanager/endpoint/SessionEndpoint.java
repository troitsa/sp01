package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.dto.SessionDTO;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.jws.WebService;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Override
    public void removeSession(@Nullable final String token) throws Exception {
        sessionService.remove(token);
    }

    @Override
    public @Nullable SessionDTO findOneSession(@Nullable final String id) {
        return toSessionDTO(sessionService.findOne(id));
    }

    @Nullable
    @Override
    public String getToken (@Nullable final String login, @Nullable String password) throws Exception {
        return sessionService.getToken(login, password);
    }

    @Override
    @Nullable
    public String getCurrentUserId(@Nullable final String token) throws Exception {
        return sessionService.getCurrentUserId(token);
    }

    @Nullable
    private SessionDTO toSessionDTO(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setRole(session.getRole());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setCreateDate(session.getCreateDate());
        return sessionDTO;
    }

    @Nullable
    private Session toSession(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final Session session = new Session();
        session.setId(sessionDTO.getId());
        User user = userService.findOne(sessionDTO.getUserId());
        if (user == null) return null;
        session.setUser(user);
        session.setRole(sessionDTO.getRole());
        session.setSignature(sessionDTO.getSignature());
        session.setCreateDate(sessionDTO.getCreateDate());
        return session;
    }
}
