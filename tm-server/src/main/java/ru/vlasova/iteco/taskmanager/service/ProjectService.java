package ru.vlasova.iteco.taskmanager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Setter
@Service
@Transactional
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @Nullable
    public Project insert(@Nullable final User user, @Nullable final String name,
                          @Nullable final String description, @Nullable final String dateStart,
                          @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || user == null) return null;
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUtil.parseDateFromString(dateStart));
        project.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) {
        if (userId == null) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final Project project = getProjectByIndex(userId, index);
        if (project == null) return;
        repository.remove(userId, project.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        repository.remove(userId, id);
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        repository.removeById(id);
    }

    @Override
    @Nullable
    public Project getProjectByIndex(@Nullable final String userId, final int index) {
        if (userId == null || index < 0) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<Project> projectList = repository.findAllByUserId(userId);
        return projectList.get(index);
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectIndex(@Nullable final String userId, final int projectIndex) {
        if (userId == null || projectIndex < 0) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<Project> projectList = repository.findAllByUserId(userId);
        @Nullable final String projectId = projectList.get(projectIndex).getId();
        return taskService.getTasksByProjectId(userId, projectId);
    }

    @Override
    @Nullable
    public List<Project> search(@Nullable final String userId, @Nullable final String searchString) {
        if (userId == null || searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<Project> projectList = repository.search(userId, searchString);
        return projectList;
    }

    @Override
    @Nullable
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<Project> projectList = repository.findAllByUserId(userId);
        return projectList;
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        repository.merge(project);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        repository.persist(project);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<Project> projectList = repository.findAll();
        return projectList;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final Project project = repository.findOne(id);
        return project;
    }

    @Override
    @Nullable
    public Project findOneByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final Project project = repository.findOneByUserId(userId, id);
        return project;
    }

    @Override
    public void removeAll() {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        repository.removeAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        repository.removeAllByUserId(userId);
    }

    @Override
    public @Nullable List<Project> sortProject(@Nullable final String userId,
                                               @Nullable final String sortMode) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
         @NotNull final List<Project> projects = repository.sortProject(userId, sortMode);
        return projects;
    }

}