package ru.vlasova.iteco.taskmanager.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.entity.User;

import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository implements IUserRepository {

    @NotNull
    @Override
    public String checkUser(@NotNull final String login) {
        @Nullable final User user = em.createQuery("SELECT u FROM app_user u WHERE u.login = :login",
                User.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("login", login).setMaxResults(1)
                .getSingleResult();
        return user.getId();
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
         List list = em.createQuery("SELECT u FROM app_user u WHERE  u.id = :id",
                User.class)
                 .setHint(QueryHints.CACHEABLE, true)
                 .setParameter("id", id).setMaxResults(1).getResultList();
        return list.isEmpty() ? null : (User)list.get(0);
    }

    @Override
    public void persist(@NotNull final User user) {
        em.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        em.merge(user);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM app_user ").executeUpdate();
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return em.createQuery("SELECT u FROM app_user u", User.class)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        em.createQuery("DELETE FROM app_user u WHERE u.id = :id")
                .setParameter("id", id).executeUpdate();
    }

}