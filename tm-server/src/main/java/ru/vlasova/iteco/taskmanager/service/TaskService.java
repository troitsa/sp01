package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final List<Task> taskList = repository.getTasksByProjectId(userId, projectId);
        return taskList;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final User user, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || user == null) return null;
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) {
        if (userId == null) return;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @Nullable final Task task = getTaskByIndex(userId, index);
        if (task == null) return;
        repository.removeByUserId(userId, task.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        repository.removeByUserId(userId, id);
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@Nullable final String userId, final int index) {
        if (userId == null || index < 0 ) return null;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        return taskList.get(index);
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String userId, @Nullable final String searchString) {
        if (userId == null || searchString == null || searchString.isEmpty()) return null;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final List<Task> taskList = repository.search(userId, searchString);
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        return taskList;
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        repository.merge(task);
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        repository.persist(task);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final List<Task> taskList = repository.findAll();
        return taskList;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @Nullable final Task task = repository.findOne(id);
        return task;
    }

    @Override
    @Nullable
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @Nullable final Task task = repository.findOneByUserId(userId, id);
        return task;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        repository.remove(id);
    }

    @Override
    public void removeAll() {
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        repository.removeAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        repository.removeAllByUserId(userId);
    }

    @Override
    public @Nullable List<Task> sortTask(@Nullable final String userId,
                                         @Nullable final String sortMode) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final List<Task> taskList = repository.sortTask(userId, sortMode);
        return taskList;
    }

}
