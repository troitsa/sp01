package ru.vlasova.iteco.taskmanager.repository;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.entity.Session;

import java.util.List;

@Repository
@NoArgsConstructor
@Scope("prototype")
public final class SessionRepository extends AbstractRepository implements ISessionRepository {

    @Override
    public @NotNull List<Session> findAll() {
        return em.createQuery("SELECT s FROM app_task s", Session.class)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) {
        return em.find(Session.class, id);
    }

    @Override
    public void remove(@NotNull final String id) {
        em.createQuery("DELETE FROM app_session s WHERE s.id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void persist(@NotNull final Session session) {
        em.persist(session);
    }

    @Override
    public void merge(@NotNull final Session session) {
        em.merge(session);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM app_session").executeUpdate();
    }

    @Override
    public void removeSessionBySignature(@NotNull final String signature) {
        em.createQuery("DELETE FROM app_session s WHERE s.signature = :signature")
                .setParameter("signature", signature).executeUpdate();
    }

    @Override
    public boolean contains(@NotNull final String id) {
        @Nullable final Session session = em.find(Session.class, id);
        return session != null;
    }

}
