package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public interface ISessionRepository {

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findOne(@NotNull final String id);

    void persist(@NotNull final Session obj);

    void merge(@NotNull final Session obj);

    void remove(@NotNull final String id);

    void removeAll();

    void removeSessionBySignature(@NotNull final String signature);

    boolean contains(@NotNull final String sessionId);

}
