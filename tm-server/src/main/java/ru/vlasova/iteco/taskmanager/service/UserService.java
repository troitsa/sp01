package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    public void edit(@Nullable final User user,
                     @Nullable final String login,
                     @Nullable final String password) {
        if (!isValid(login, password) || user == null) return;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        user.setLogin(login);
        user.setPwd(HashUtil.MD5(password));
        repository.merge(user);
    }

    @Override
    @Nullable
    public User insert(@Nullable final String login, @Nullable final String password) {
        final boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    @Override
    @Nullable
    public User doLogin(final String login, final String password) throws Exception {
        @Nullable final String id = checkUser(login);
        if (id == null) return null;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        @Nullable final User user = repository.findOne(id);
        if (user == null) return null;
        @Nullable final String pwd = user.getPwd();
        if (pwd == null) return null;
        if (pwd.equals(HashUtil.MD5(password))) {
            return user;
        } else {
            throw new AccessDeniedException("User or password incorrect");
        }
    }

    @Override
    @Nullable
    public String checkUser(final String login) {
        if (!isValid(login)) return null;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        @Nullable String id = repository.checkUser(login);
        return id;
    }

    @Override
    public boolean checkRole(@Nullable final String userId, @Nullable final List<Role> roles) {
        if (userId == null || roles == null) return false;
        @Nullable User user = findOne(userId);
        if (user == null) return false;
        for (@Nullable Role role : roles) {
            if (user.getRole().equals(role)) return true;
        }
        return false;
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        repository.merge(user);
    }

    @Override
    public void persist(@Nullable final User user)  {
        if (user == null) return;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        repository.persist(user);
    }

    @Override
    @NotNull
    public List<User> findAll() {
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        @NotNull final List<User> users = repository.findAll();
        return users;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null) return null;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        @Nullable final User user = repository.findOne(id);
        return user;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        repository.remove(id);
    }

    @Override
    public void removeAll() {
        @NotNull final IUserRepository repository = context.getBean(UserRepository.class);
        repository.removeAll();
    }

}
