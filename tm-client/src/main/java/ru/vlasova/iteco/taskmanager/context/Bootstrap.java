package ru.vlasova.iteco.taskmanager.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.UserDTO;
import ru.vlasova.iteco.taskmanager.api.service.IStateService;
import ru.vlasova.iteco.taskmanager.api.service.ITerminalService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.Map;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private IStateService stateService;

    @NotNull
    @Autowired
    private ITerminalService terminalService;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    private void registry(@NotNull final AbstractCommand command) {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();

        if (cliCommand.isEmpty()) throw new RuntimeException("cliCommand is empty");

        if (cliDescription.isEmpty()) throw new RuntimeException("cliDescription is empty");

        stateService.add(command);
    }

    public void start() {
        createUsers();
        terminalService.print("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";

        while (true) {
            command = terminalService.readString();
            execute(command);
        }
    }

    private void execute(@Nullable final String command) {
        try {
            @Nullable final AbstractCommand abstractCommand = stateService.get(command);
            if (abstractCommand == null) return;
            @NotNull final boolean checkAccess = !abstractCommand.secure() || abstractCommand.secure() &&
                    stateService.getToken() != null;
            if (checkAccess) {
                abstractCommand.execute();
            } else {
                terminalService.print("Access denied. Please, login");
            }
        } catch (Exception e) {
            terminalService.print(e.getMessage());
        }

    }

    @NotNull
    public Bootstrap init() {
        @NotNull final Map<String, AbstractCommand> classes = context.getBeansOfType(AbstractCommand.class);
        classes.values().stream().forEach(this::registry);
        return this;
    }

    private void createUsers() {
        try {
            @NotNull final UserDTO user = userEndpoint.insertUser("user", "qwerty");
            user.setRole(Role.USER);

            @NotNull final UserDTO admin = userEndpoint.insertUser("admin", "1234");
            admin.setRole(Role.ADMIN);
            userEndpoint.persistUser(user);
            userEndpoint.persistUser(admin);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}