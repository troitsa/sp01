package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.UserDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

@Component
public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_login";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User authorization";
    }

    @Override
    public void execute() throws Exception {
        terminalService.print("Login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Password: ");
        @Nullable final String password = terminalService.readString();
        if(login == null || password == null ) {
            terminalService.print("Login or password invalid");
            return;
        }
        @Nullable final UserDTO user = userEndpoint.doLogin(login, password);
        if(user != null) {
            String token = sessionEndpoint.getToken(login,password);
            stateService.setToken(token);
            terminalService.print("Log in success");
        }
        else {
            terminalService.print("Login or password invalid");
        }
    }

}