package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Component
public final class ProjectSortCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "project_sort";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sorting projects";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        terminalService.print("Choose sorting option:\n" +
                "1 sorting by creating date\n"+
                "2 sorting by starting date\n"+
                "3 sorting by finish date\n"+
                "4 sorting by ready condition\n"+
                "or leave the field empty");
        @Nullable final String sortMode = terminalService.readString();
        Stream<ProjectDTO> stream = projectEndpoint.sortProject(userId, sortMode).stream();
        stream.forEach(e -> terminalService.print(e.getName()));
    }

}
