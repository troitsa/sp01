package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class TaskDetachCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_detach";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Detach task from project";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        terminalService.print("Choose the task and type the number");
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAllTasksByUserId(token, userId);
        if(taskList == null || taskList.size() == 0)
        printTaskList(taskList);
        final int index = Integer.parseInt(terminalService.readString()) - 1;
        @Nullable final TaskDTO task = taskEndpoint.getTaskByIndex(token, userId, index);
        if (task == null) return;
        task.setProjectId("");
        terminalService.print("Task detached.");
    }

}