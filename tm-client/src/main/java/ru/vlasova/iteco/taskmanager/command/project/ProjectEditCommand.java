package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class ProjectEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "project_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit selected project";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        terminalService.print("Choose the project and type the number");
        printProjectList(userId);
        @Nullable final int index = Integer.parseInt(terminalService.readString())-1;
        @Nullable final ProjectDTO project = projectEndpoint.getProjectByIndex(token, userId, index);
        if(project == null) return;
        terminalService.print("Editing project: " + project.getName() + ". Set new name: ");
        project.setName(terminalService.readString());
        projectEndpoint.mergeProject(token, project);
        terminalService.print("Project edit.");
    }

}