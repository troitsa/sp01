package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.List;

public interface IStateService {

    @NotNull List<AbstractCommand> getCommands();

    void add(@NotNull final AbstractCommand command);

    AbstractCommand get(@Nullable final String command);

    @Nullable
    String getToken();

    void setToken(String token);

}
