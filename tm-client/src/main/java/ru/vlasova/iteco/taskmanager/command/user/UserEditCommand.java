package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.UserDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit user";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        terminalService.print("Edit user. Set new login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Set new password: ");
        @Nullable final String password = terminalService.readString();
        @NotNull final UserDTO user = userEndpoint.findUserBySession(token, userId);
        userEndpoint.editUser(token, user, login, password);
        terminalService.print("User saved");
    }

}