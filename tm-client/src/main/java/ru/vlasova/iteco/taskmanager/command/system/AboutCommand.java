package ru.vlasova.iteco.taskmanager.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

@Component
public final class AboutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "about";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Print about build";
    }

    @Override
    public void execute() {
        @Nullable final String buildNumber = Manifests.read("buildNumber");
        @Nullable final String developer = Manifests.read("developer");
        terminalService.print("build number: " + buildNumber + "\n" + "developer: " + developer);
    }

}
