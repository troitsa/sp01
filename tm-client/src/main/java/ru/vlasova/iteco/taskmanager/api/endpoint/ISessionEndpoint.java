package ru.vlasova.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-06-02T13:33:28.449+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/getTokenRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/getTokenResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/getToken/Fault/Exception")})
    @RequestWrapper(localName = "getToken", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.GetToken")
    @ResponseWrapper(localName = "getTokenResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.GetTokenResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getToken(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/getCurrentUserIdRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/getCurrentUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/getCurrentUserId/Fault/Exception")})
    @RequestWrapper(localName = "getCurrentUserId", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.GetCurrentUserId")
    @ResponseWrapper(localName = "getCurrentUserIdResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.GetCurrentUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getCurrentUserId(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/removeSessionRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/removeSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/removeSession/Fault/Exception")})
    @RequestWrapper(localName = "removeSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.RemoveSession")
    @ResponseWrapper(localName = "removeSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.RemoveSessionResponse")
    public void removeSession(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/findOneSessionRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/findOneSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/findOneSession/Fault/Exception")})
    @RequestWrapper(localName = "findOneSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.FindOneSession")
    @ResponseWrapper(localName = "findOneSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.FindOneSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vlasova.iteco.taskmanager.api.endpoint.SessionDTO findOneSession(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    ) throws Exception_Exception;
}
