
package ru.vlasova.iteco.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.vlasova.iteco.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "Exception");
    private final static QName _CheckRole_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "checkRole");
    private final static QName _CheckRoleResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "checkRoleResponse");
    private final static QName _CheckUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "checkUser");
    private final static QName _CheckUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "checkUserResponse");
    private final static QName _DoLogin_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "doLogin");
    private final static QName _DoLoginResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "doLoginResponse");
    private final static QName _EditUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "editUser");
    private final static QName _EditUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "editUserResponse");
    private final static QName _FindAllUsers_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "findAllUsers");
    private final static QName _FindAllUsersResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "findAllUsersResponse");
    private final static QName _FindUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "findUser");
    private final static QName _FindUserBySession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "findUserBySession");
    private final static QName _FindUserBySessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "findUserBySessionResponse");
    private final static QName _FindUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "findUserResponse");
    private final static QName _InsertUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "insertUser");
    private final static QName _InsertUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "insertUserResponse");
    private final static QName _MergeUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "mergeUser");
    private final static QName _MergeUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "mergeUserResponse");
    private final static QName _PersistUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "persistUser");
    private final static QName _PersistUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "persistUserResponse");
    private final static QName _RemoveAllUsers_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "removeAllUsers");
    private final static QName _RemoveAllUsersResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "removeAllUsersResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "removeUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "removeUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.vlasova.iteco.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CheckRole }
     * 
     */
    public CheckRole createCheckRole() {
        return new CheckRole();
    }

    /**
     * Create an instance of {@link CheckRoleResponse }
     * 
     */
    public CheckRoleResponse createCheckRoleResponse() {
        return new CheckRoleResponse();
    }

    /**
     * Create an instance of {@link CheckUser }
     * 
     */
    public CheckUser createCheckUser() {
        return new CheckUser();
    }

    /**
     * Create an instance of {@link CheckUserResponse }
     * 
     */
    public CheckUserResponse createCheckUserResponse() {
        return new CheckUserResponse();
    }

    /**
     * Create an instance of {@link DoLogin }
     * 
     */
    public DoLogin createDoLogin() {
        return new DoLogin();
    }

    /**
     * Create an instance of {@link DoLoginResponse }
     * 
     */
    public DoLoginResponse createDoLoginResponse() {
        return new DoLoginResponse();
    }

    /**
     * Create an instance of {@link EditUser }
     * 
     */
    public EditUser createEditUser() {
        return new EditUser();
    }

    /**
     * Create an instance of {@link EditUserResponse }
     * 
     */
    public EditUserResponse createEditUserResponse() {
        return new EditUserResponse();
    }

    /**
     * Create an instance of {@link FindAllUsers }
     * 
     */
    public FindAllUsers createFindAllUsers() {
        return new FindAllUsers();
    }

    /**
     * Create an instance of {@link FindAllUsersResponse }
     * 
     */
    public FindAllUsersResponse createFindAllUsersResponse() {
        return new FindAllUsersResponse();
    }

    /**
     * Create an instance of {@link FindUser }
     * 
     */
    public FindUser createFindUser() {
        return new FindUser();
    }

    /**
     * Create an instance of {@link FindUserBySession }
     * 
     */
    public FindUserBySession createFindUserBySession() {
        return new FindUserBySession();
    }

    /**
     * Create an instance of {@link FindUserBySessionResponse }
     * 
     */
    public FindUserBySessionResponse createFindUserBySessionResponse() {
        return new FindUserBySessionResponse();
    }

    /**
     * Create an instance of {@link FindUserResponse }
     * 
     */
    public FindUserResponse createFindUserResponse() {
        return new FindUserResponse();
    }

    /**
     * Create an instance of {@link InsertUser }
     * 
     */
    public InsertUser createInsertUser() {
        return new InsertUser();
    }

    /**
     * Create an instance of {@link InsertUserResponse }
     * 
     */
    public InsertUserResponse createInsertUserResponse() {
        return new InsertUserResponse();
    }

    /**
     * Create an instance of {@link MergeUser }
     * 
     */
    public MergeUser createMergeUser() {
        return new MergeUser();
    }

    /**
     * Create an instance of {@link MergeUserResponse }
     * 
     */
    public MergeUserResponse createMergeUserResponse() {
        return new MergeUserResponse();
    }

    /**
     * Create an instance of {@link PersistUser }
     * 
     */
    public PersistUser createPersistUser() {
        return new PersistUser();
    }

    /**
     * Create an instance of {@link PersistUserResponse }
     * 
     */
    public PersistUserResponse createPersistUserResponse() {
        return new PersistUserResponse();
    }

    /**
     * Create an instance of {@link RemoveAllUsers }
     * 
     */
    public RemoveAllUsers createRemoveAllUsers() {
        return new RemoveAllUsers();
    }

    /**
     * Create an instance of {@link RemoveAllUsersResponse }
     * 
     */
    public RemoveAllUsersResponse createRemoveAllUsersResponse() {
        return new RemoveAllUsersResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "checkRole")
    public JAXBElement<CheckRole> createCheckRole(CheckRole value) {
        return new JAXBElement<CheckRole>(_CheckRole_QNAME, CheckRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "checkRoleResponse")
    public JAXBElement<CheckRoleResponse> createCheckRoleResponse(CheckRoleResponse value) {
        return new JAXBElement<CheckRoleResponse>(_CheckRoleResponse_QNAME, CheckRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "checkUser")
    public JAXBElement<CheckUser> createCheckUser(CheckUser value) {
        return new JAXBElement<CheckUser>(_CheckUser_QNAME, CheckUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "checkUserResponse")
    public JAXBElement<CheckUserResponse> createCheckUserResponse(CheckUserResponse value) {
        return new JAXBElement<CheckUserResponse>(_CheckUserResponse_QNAME, CheckUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "doLogin")
    public JAXBElement<DoLogin> createDoLogin(DoLogin value) {
        return new JAXBElement<DoLogin>(_DoLogin_QNAME, DoLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "doLoginResponse")
    public JAXBElement<DoLoginResponse> createDoLoginResponse(DoLoginResponse value) {
        return new JAXBElement<DoLoginResponse>(_DoLoginResponse_QNAME, DoLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "editUser")
    public JAXBElement<EditUser> createEditUser(EditUser value) {
        return new JAXBElement<EditUser>(_EditUser_QNAME, EditUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "editUserResponse")
    public JAXBElement<EditUserResponse> createEditUserResponse(EditUserResponse value) {
        return new JAXBElement<EditUserResponse>(_EditUserResponse_QNAME, EditUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "findAllUsers")
    public JAXBElement<FindAllUsers> createFindAllUsers(FindAllUsers value) {
        return new JAXBElement<FindAllUsers>(_FindAllUsers_QNAME, FindAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "findAllUsersResponse")
    public JAXBElement<FindAllUsersResponse> createFindAllUsersResponse(FindAllUsersResponse value) {
        return new JAXBElement<FindAllUsersResponse>(_FindAllUsersResponse_QNAME, FindAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "findUser")
    public JAXBElement<FindUser> createFindUser(FindUser value) {
        return new JAXBElement<FindUser>(_FindUser_QNAME, FindUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserBySession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "findUserBySession")
    public JAXBElement<FindUserBySession> createFindUserBySession(FindUserBySession value) {
        return new JAXBElement<FindUserBySession>(_FindUserBySession_QNAME, FindUserBySession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserBySessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "findUserBySessionResponse")
    public JAXBElement<FindUserBySessionResponse> createFindUserBySessionResponse(FindUserBySessionResponse value) {
        return new JAXBElement<FindUserBySessionResponse>(_FindUserBySessionResponse_QNAME, FindUserBySessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "findUserResponse")
    public JAXBElement<FindUserResponse> createFindUserResponse(FindUserResponse value) {
        return new JAXBElement<FindUserResponse>(_FindUserResponse_QNAME, FindUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "insertUser")
    public JAXBElement<InsertUser> createInsertUser(InsertUser value) {
        return new JAXBElement<InsertUser>(_InsertUser_QNAME, InsertUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "insertUserResponse")
    public JAXBElement<InsertUserResponse> createInsertUserResponse(InsertUserResponse value) {
        return new JAXBElement<InsertUserResponse>(_InsertUserResponse_QNAME, InsertUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "mergeUser")
    public JAXBElement<MergeUser> createMergeUser(MergeUser value) {
        return new JAXBElement<MergeUser>(_MergeUser_QNAME, MergeUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "mergeUserResponse")
    public JAXBElement<MergeUserResponse> createMergeUserResponse(MergeUserResponse value) {
        return new JAXBElement<MergeUserResponse>(_MergeUserResponse_QNAME, MergeUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "persistUser")
    public JAXBElement<PersistUser> createPersistUser(PersistUser value) {
        return new JAXBElement<PersistUser>(_PersistUser_QNAME, PersistUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "persistUserResponse")
    public JAXBElement<PersistUserResponse> createPersistUserResponse(PersistUserResponse value) {
        return new JAXBElement<PersistUserResponse>(_PersistUserResponse_QNAME, PersistUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "removeAllUsers")
    public JAXBElement<RemoveAllUsers> createRemoveAllUsers(RemoveAllUsers value) {
        return new JAXBElement<RemoveAllUsers>(_RemoveAllUsers_QNAME, RemoveAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "removeAllUsersResponse")
    public JAXBElement<RemoveAllUsersResponse> createRemoveAllUsersResponse(RemoveAllUsersResponse value) {
        return new JAXBElement<RemoveAllUsersResponse>(_RemoveAllUsersResponse_QNAME, RemoveAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

}
