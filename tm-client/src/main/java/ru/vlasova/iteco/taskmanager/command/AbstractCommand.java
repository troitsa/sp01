package ru.vlasova.iteco.taskmanager.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.*;
import ru.vlasova.iteco.taskmanager.api.service.IStateService;
import ru.vlasova.iteco.taskmanager.api.service.ITerminalService;

import java.lang.Exception;
import java.util.List;

@Component
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    public IStateService stateService;

    @NotNull
    @Autowired
    public ITerminalService terminalService;

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected ISessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    protected String token;

    @Getter
    protected String userId;

    public abstract boolean secure();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public List<Role> getRole() {
        return null;
    }

    protected void printProjectList(@Nullable final String userId) throws Exception {
        @Nullable final List<ProjectDTO> projectList = projectEndpoint.findAllProjectsByUserId(token, userId);
        if (projectList == null || projectList.size() == 0) {
            terminalService.print("There are no projects.");
            return;
        }
        int i = 1;
        for (@NotNull ProjectDTO project : projectList) {
            terminalService.print(i++ + ": " + project.getName());
        }
    }

    public void printTaskList(@Nullable final List<TaskDTO> taskList) {
        int i = 1;
        if (taskList == null) return;
        for (@Nullable TaskDTO task : taskList) {
            terminalService.print(i++ + ": " + task.getName());
        }
    }

    public void validSession() throws Exception {
        token = stateService.getToken();
        if(token == null) {
            terminalService.print("Access denied");
            return;
        }
        userId = sessionEndpoint.getCurrentUserId(token);
        @Nullable final List<Role> roles = getRole();
        @NotNull final boolean checkRole = roles == null ||
                userEndpoint.checkRole(userId, roles);
        if(!checkRole) throw new Exception("Access denied!");
    }

}